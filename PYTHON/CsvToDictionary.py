import csv

class CsvUtility( object ):

    # Read a CSV file and store its data in a dictionary
    @staticmethod
    def CsvToDictionary( filepath ):
        fileContents = csv.DictReader( open( filepath ) )
        return fileContents

