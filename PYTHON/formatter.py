from CsvUtility import CsvUtility

print( "\n\n" )

path_LaadanToEnglish = "../DATA/laadan-to-english.csv"
path_LaadanToSpanish = "../DATA/laadan-to-spanish.csv"

data_LaadanToEnglish = CsvUtility.CsvToDictionary( path_LaadanToEnglish )
data_LaadanToSpanish = CsvUtility.CsvToDictionary( path_LaadanToSpanish )

laadanEnglish = {}
laadanSpanish = {}

for row in data_LaadanToEnglish:
    laadanEnglish[row["láadan"]] = row["english"]

for row in data_LaadanToSpanish:
    laadanSpanish[row["láadan"]] = row["español"]


# Create CSV file
outputCsv = open( "formatted.csv", "w" )

outputCsv.write( "láadan,english,spanish\n" )

for laadan, english in laadanEnglish.items():

    if ( laadan in laadanSpanish ):
        spanish = laadanSpanish[ laadan ]
    else:
        spanish = ""

    outputCsv.write( laadan + "," + english + "," )
    outputCsv.write( spanish + "\n" )


print( "\n\n" )
