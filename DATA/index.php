<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  
  <?
  /*
  password r5hFQeiE!
    Database
    dbs161612
    Description
    laadan-database

    Host name
    db5000166551.hosting-data.io

    Port
    3306
    User name
    dbu218427
    Type and version
    MySql 5.7
    * */
    //$server = "db5000166551.hosting-data.io";
    //$username = "dbu218427";
    //$password = "r5hFQeiE!";
    
    //$dbConnection = new mysqli( $server, $username, $password );
    
    //if ( $dbConnection->connect_error ) {
        //echo( "Connection failed" );
    //}
    
    //$getAllEntriesQuery = "SELECT * FROM `LaadanDictionary` WHERE 1";
    //$allEntries = $dbConnection->query( $getAllEntriesQuery );
    
    //if ( $allEntries->num_rows > 0 ) {
        //echo( "Returned" );
    //}
    //else {
        //echo( "None" );
    //}
    
    //$a = $allEntries->fetch_assoc();
    //echo( $a["laadan"] . ", " . $a["english"] );
  ?>

  <title> Editable Láadan dictionary - tools.ayadanconlangs.com </title>
  <meta name="description" content="Computer-readable conlang data for use to build APIs and other products">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="basic.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <? include_once( "reader.php" ); ?>
    <? $data = Read(); ?>
    
    <style type="text/css">
        .dictionary-entry { margin: 20px; border: solid 2px #000; border-radius: 15px; }
        .dictionary-entry input, .dictionary-entry textarea { margin: 10px; }
    .edit-view {}
    .edit-view label { font-size: 10pt; margin: 0; padding: 0; }
    .collapsed { display: none; }
    
    .gridbox { border: solid 1px #aaa; border-radius: 15px; }
    .gridbox .summary { text-align: center; }
    
    input { margin: 5px 0; }
    </style>
   
    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
      
    <script>
        $( document ).ready( function() {
            $( ".word-entry" ).click( function() {
                if ( $( this ).next().css( "display" ) == "none" ) {
                    $( this ).next( ".edit-view" ).slideDown( "fast" );
                }
                else {
                    $( this ).next( ".edit-view" ).slideUp( "fast" );
                }
            } );
        } );
    </script>
</head>

<body>

<div class="container">
    <p> Editable Láadan dictionary @ ayadanconlangs.com </p>
    
    <p>
        <a href="">Download as LaTeX .tex file</a> | 
        <a href="">Download as JSON</a> | 
        <a href="">Download as CSV</a>
    </p>
    
    <p>
        Last update: 
        <?= date( "Y-m-d", filemtime( "laadan-to-english.csv" ) ); ?>
    </p>
    
    <table class="table">
    <? 
        $currentCategory = "";
        
        foreach( $data as $key => $entry ) { 
        if ( $key == 0 ) { continue; }
        
        ?>
        <tr class="word-entry">
            <td><?=$key?></td>
            <td><?=$entry[0]?></td>
            <td><?=$entry[1]?></td>
        </tr>
        <tr class="edit-view collapsed">
            <td colspan="3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 entry-laadan">
                            <label>Láadan</label><br>
                            <input type="text" value="<?=$entry[0]?>" name="entry-laadan-<?=$key?>" class="form-control">
                        </div>
                        <div class="col-md-6 entry-english">
                            <label>English</label><br>
                            <input type="text" value="<?=$entry[1]?>" name="entry-english-<?=$key?>" class="form-control">
                        </div>
                        <div class="col-md-12 entry-description">
                            <label>Description</label><br>
                            <textarea class="form-control"><?=$entry[2]?></textarea>
                        </div>
                        <div class="col-md-6 entry-classification">
                            <label>Classification</label><br>
                            <input type="text" value="<?=$entry[3]?>" name="entry-classification-<?=$key?>" class="form-control">
                        </div>
                        <div class="col-md-6 entry-breakdown">
                            <label>Breakdown</label><br>
                            <input type="text" value="<?=$entry[4]?>" name="entry-wordbreakdown-<?=$key?>" class="form-control">
                        </div>
                        <div class="col-md-12 entry-notes">
                            <label>Notes</label><br>
                            <textarea class="form-control"><?=$entry[5]?></textarea>
                        </div>
                        <div class="col-md-12 entry-latex">
                            <label>LaTeX</label><br>
                            Later...
                        </div>
                        <div class="col-md-10 entry">
                        </div>
                        <div class="col-md-2 entry-save">
                            <input type="submit" class="btn btn-primary form-control" value="Save">
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        
    <? } ?>
    </table>
</div>


    <!--

    <div class="row summary-view">
        <div class="col-md-1">
    </div>

    <div class="row dictionary-entry">
        <div class="col-md-6 entry-laadan">
            <input type="text" value="<?=$entry[0]?>" name="entry-laadan-<?=$key?>" class="form-control">
        </div>
        <div class="col-md-6 entry-english">
            <input type="text" value="<?=$entry[1]?>" name="entry-english-<?=$key?>" class="form-control">
        </div>
        <div class="col-md-12 entry-description">
            <textarea class="form-control"><?=$entry[2]?></textarea>
        </div>
        <div class="col-md-6 entry-classification">
            <input type="text" value="<?=$entry[3]?>" name="entry-classification-<?=$key?>" class="form-control">
        </div>
        <div class="col-md-6 entry-breakdown">
            <input type="text" value="<?=$entry[4]?>" name="entry-wordbreakdown-<?=$key?>" class="form-control">
        </div>
        <div class="col-md-12 entry-notes">
            <textarea class="form-control"><?=$entry[5]?></textarea>
        </div>
    </div>
-->
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
